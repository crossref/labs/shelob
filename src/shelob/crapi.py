import json

from claws.aws_utils import AWSConnector


class CrossrefAPI:
    def __init__(
        self,
        bucket="samples.research.crossref.org",
        aws_connector: AWSConnector = None,
        region_name="us-east-1",
    ):
        self._bucket = bucket
        self._aws_connector = (
            AWSConnector(region_name=region_name, unsigned=True, bucket=bucket)
            if not aws_connector
            else aws_connector
        )

    def get_most_recent_samples(self):
        most_recent_prefix = self.list_prefix("all-works/")[-1]
        file = f"{most_recent_prefix['Prefix']}sample.jsonl"

        samples = self._aws_connector.s3_obj_to_str(
            bucket=self._bucket, s3_path=file, raise_on_fail=False
        )

        result = [json.loads(jline) for jline in samples.splitlines()]

        return result

    def list_prefix(self, prefix: str):
        result = self._aws_connector.s3_client.list_objects(
            Bucket=self._bucket, Delimiter="/", Prefix=prefix
        )
        return result.get("CommonPrefixes", list())
