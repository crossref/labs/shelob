import datetime
import hashlib
import io
import json
import operator
import os
import uuid
from importlib import import_module
from json import JSONDecodeError

import clannotation
import credcheck.cred
import html2text
import httpx
import magic
from bs4 import BeautifulSoup, FeatureNotFound
from clannotation.annotator import Annotator
from claws.aws_utils import AWSConnector, S3ObjException
from jinja2 import Template
from pypdf import PdfReader
from rich import print

import settings


class Shelob:
    _aws_connector: AWSConnector = None

    def __init__(
        self,
        bucket="outputs.research.crossref.org",
        shelob_bucket="shelob.research.crossref.org",
        aws_connector: AWSConnector = None,
        region_name="eu-west-2",
        crossref_api_endpoint="https://api.crossref.org",
    ):
        self._chromium = None
        self._browser = None
        self._page = None
        self.crossref_api_endpoint = crossref_api_endpoint
        self._bucket = bucket
        self._shelob_bucket = shelob_bucket
        self._aws_connector = (
            AWSConnector(
                region_name=region_name, unsigned=False, bucket=self._bucket
            )
            if not aws_connector
            else aws_connector
        )

        self._shelob_aws_connector = (
            AWSConnector(
                region_name=region_name,
                unsigned=False,
                bucket=self._shelob_bucket,
            )
            if not aws_connector
            else aws_connector
        )

    @staticmethod
    def fetch(url: str, timeout: int = 30) -> httpx.Response:
        """
        Download a remote address
        :param url: the URL to fetch
        :param timeout: the timeout in seconds
        :return: the response
        """

        try:
            with httpx.Client(
                timeout=timeout, follow_redirects=True, verify=False
            ) as client:
                return client.get(url)
        except (
            httpx.RemoteProtocolError,
            httpx.ReadTimeout,
            httpx.ConnectError,
            httpx.ConnectTimeout,
        ) as e:
            return httpx.Response(status_code=407, content=e)

    def _get_title_from_crossref(self, doi: str) -> str:
        """
        Get the title from the Crossref REST API
        :param doi: the DOI to fetch
        :return: the title
        """
        response = self.fetch(f"{self.crossref_api_endpoint}/works/{doi}")

        try:
            message = response.json()["message"]
        except JSONDecodeError:
            return ""

        title_json = (
            message["message"]["title"]
            if "title" in response.json()["message"]
            else []
        )
        return title_json[0] if len(title_json) > 0 else ""

    def snapshot(
        self,
        url: str,
        doi: str = "",
        title: str = "",
        strategy="httpx",
        record=None,
        quiet=False,
    ):
        """
        Take a snapshot of a page
        :param url: the URL to snapshot
        :param doi: the DOI associated with the page
        :param title: the title associated with the page
        :param strategy: the strategy to use to fetch the URL
        :param record: the associated record
        :param quiet: suppress output
        :return: the snapshot
        """
        if title == "":
            # fetch the title from the Crossref REST API
            title = self._get_title_from_crossref(doi)

        if type(title) is list:
            title = title[0]

        # fetch the URL
        if strategy.lower() == "httpx":
            response = self.fetch(url)
        else:
            raise NotImplementedError

        if response.status_code != 200:
            endpoint_response = self._build_endpoint_snapshot(
                url=url,
                doi=doi,
                title=title,
                datestamp=datetime.datetime.now(),
                tags=[],
                status_code=response.status_code,
                strategy=strategy,
            )
            return endpoint_response

        # generate the checksum of response.content using SHA256
        sha256 = hashlib.sha256(response.content).hexdigest()

        # get the mime type of the response
        mime = self.get_mime(response.content)

        # determine if the DOI and title are in the document
        found_doi, found_title = self._has_doi_and_title(
            content=response.content, doi=doi, title=title, mime=mime
        )

        # if the doi and the URL are the same, this is a landing page
        # we can attempt to profile it to see if there's a PDF link
        if doi == url and mime == "text/html":
            page_type = "Landing Page"
            pdf_citation = self._find_pdf(response.content)
            pdf_strategy = "Meta tag"
        else:
            pdf_citation = ""
            page_type = "Fulltext"
            pdf_strategy = ""

        record = self.get_history_record(locator=doi, quiet=False)

        tags = self._set_tags(found_doi, found_title, record)

        endpoint_response = self._build_endpoint_snapshot(
            url=url,
            doi=doi,
            title=title,
            checksum=sha256,
            datestamp=datetime.datetime.now(),
            tags=tags,
            status_code=response.status_code,
            title_found=found_title,
            doi_found=found_doi,
            page_type=page_type,
            pdf_url=pdf_citation,
            pdf_strategy=pdf_strategy,
            strategy=strategy,
        )

        return endpoint_response

    @staticmethod
    def _find_pdf(content: bytes) -> str:
        """
        Find the PDF link in HTML content
        :param content: the HTML content to search
        :return: the PDF link or an empty string
        """

        pdf_url = ""

        try:
            # Assuming 'contents' is your HTML content
            soup = BeautifulSoup(content, "html.parser")

            # Find the meta element
            meta_element = soup.find("meta", attrs={"name": "citation_pdf_url"})

            # Extract the content attribute if the element exists
            if meta_element:
                pdf_url = meta_element.get("content")
            else:
                pdf_url = ""
        except FeatureNotFound:
            print(
                "[[bold red]Error[/bold red]] The specified parser is not "
                "found. Make sure you have 'html.parser' "
                "or install 'lxml' or 'html5lib'."
            )
        except Exception as e:
            print(f"An unexpected error occurred: {e}")

        return pdf_url

    @staticmethod
    def _set_tags(
        found_doi: bool, found_title: bool, record: list | None
    ) -> list:
        tags = []

        if len(record) == 0:
            tags.append("first")
            tags.append("stable")
        if found_doi:
            tags.append("has_doi")
        if found_title:
            tags.append("has_title")

        return tags

    @staticmethod
    def get_mime(content):
        mime = magic.Magic(mime=True)
        return mime.from_buffer(content)

    @staticmethod
    def _has_doi_and_title(
        content: bytes, doi: str, title: str, mime: str = ""
    ) -> tuple[bool, bool]:
        """
        Check if the DOI exists in the PDF
        :param doi: the DOI to check
        :param content: the content of the downloaded endpoint
        :param mime: the MIME type of the content
        :return: True if the DOI exists, False otherwise
        """

        if mime == "application/pdf":
            text = Shelob._extract_text_from_pdf(content)
        elif mime == "text/html":
            text = Shelob._extract_text_from_html(content)

        else:
            text = ""

        return doi in text, Shelob._test_for_title(content=text, title=title)

    @staticmethod
    def _extract_text_from_html(content):
        try:
            return html2text.html2text(content.decode("utf-8"))
        except (UnicodeDecodeError, AssertionError):
            return ""

    @staticmethod
    def _extract_text_from_pdf(content):
        bytes_io = io.BytesIO(content)
        reader = PdfReader(bytes_io)
        text = ""

        for page in reader.pages:
            text += page.extract_text() + "\n"

        return text

    @staticmethod
    def _test_for_title(content, title) -> bool:
        """
        Test if the title exists in the content
        :param content: the input to test against
        :param title: the title to test for
        :return: True if the title exists, False otherwise
        """
        content = Shelob._sanitize(content)

        # if the title has a colon in it, take just the part before it
        if ":" in title:
            title = title.split(":")[0]

        title = Shelob._sanitize(title)

        return title in content

    @staticmethod
    def _sanitize(text: str) -> str:
        """
        Sanitize the text
        :param text: the text to sanitize
        :return: the sanitized text
        """
        # Handle colons in titles or line breaks
        if ":\n" in text:
            text = text.replace(":\n", ": \n")

        # replace all newlines in OS-independent way
        if "\n" in text:
            text = "".join(text.splitlines())

        # condense multiple spaces
        while "  " in text:
            text = text.replace("  ", " ")

        # return lowercase
        return text.lower()

    @staticmethod
    def _build_endpoint_snapshot(
        url: str,
        doi: str = "",
        title: str = "",
        datestamp: datetime = datetime.datetime.now(),
        tags: list = None,
        checksum: str = "",
        status_code: int = 200,
        title_found: bool = False,
        doi_found: bool = False,
        page_type: str = "",
        pdf_url: str = "",
        pdf_strategy: str = "",
        strategy: str = "httpx",
    ):
        """
        Build the endpoint for the snapshot
        :param url: the URL to snapshot
        :param doi: the DOI of the snapshot
        :param title: the title of the snapshot
        :param datestamp: the datestamp of the snapshot
        :param tags: the tags of the snapshot
        :param checksum: the checksum of the snapshot
        :param status_code: the status code of the snapshot
        :param title_found: True if the title was found, False otherwise
        :param doi_found: True if the DOI was found, False otherwise
        :param page_type: the type of page (e.g. Landing Page, Fulltext)
        :param pdf_url: the PDF URL
        :param pdf_strategy: the strategy used to find the PDF
        :param strategy: the strategy used to fetch the URL
        :return: the endpoint JSON
        """

        tags = [] if tags is None else tags

        return (
            {
                "datestamp": datestamp.timestamp(),
                "url": url,
                "doi": doi,
                "title": title,
                "tags": tags,
                "checksum": checksum,
                "status_code": status_code,
                "has_title": title_found,
                "has_doi": doi_found,
                "page_type": page_type,
                "pdf_url": pdf_url,
                "pdf_strategy": pdf_strategy,
                "strategy": strategy,
            }
            if status_code == 200
            else {
                "datestamp": datestamp.timestamp(),
                "url": url,
                "doi": doi,
                "title": title,
                "tags": tags,
                "status_code": status_code,
                "strategy": strategy,
            }
        )

    @staticmethod
    def _build_s3_path(locator, md5=True, prefix="") -> str:
        """
        Build the S3 path for the object
        :param locator: the locator (e.g. URL)
        :return: the S3 path
        """
        if md5:
            object_name = Annotator.doi_to_md5(locator)
        else:
            object_name = locator

        return f"{prefix}/works/{object_name}.json"

    def get_current_status(self, locator: str, quiet=False):
        """
        Get the current status from S3 (Shelob bucket)
        :param locator: the locator (e.g. URL)
        :param quiet: suppress output
        :return: the record
        """
        s3_path = self._build_s3_path(locator=locator, prefix="status")

        if not quiet:
            print(f"[[bold yellow]Fetching[/bold yellow]] {s3_path}")

        json_list = json.loads(
            self._shelob_aws_connector.s3_obj_to_str(
                bucket=self._shelob_bucket, s3_path=s3_path, raise_on_fail=False
            )
        )

        return json_list

    def _enable_mr(self, locator: str, status):
        self._shelob_aws_connector.push_json_to_s3(
            json_obj=status,
            bucket=self._shelob_bucket,
            path=self._build_s3_path(locator=locator, prefix="status"),
        )

    def get_history_record(self, locator: str, quiet=False):
        """
        Get the record from S3 (Shelob bucket)
        :param locator: the locator (e.g. URL)
        :param quiet: suppress output
        :return: the record
        """
        s3_path = self._build_s3_path(locator=locator)

        if not quiet:
            print(f"[[bold yellow]Fetching[/bold yellow]] {s3_path}")

        json_list = json.loads(
            self._shelob_aws_connector.s3_obj_to_str(
                bucket=self._shelob_bucket, s3_path=s3_path, raise_on_fail=False
            )
        )

        return json_list

    def get_metadata_record(self, locator: str, quiet=False):
        """
        Get the record from S3 (outputs bucket)
        :param locator: the locator (e.g. URL)
        :param quiet: suppress output
        :return: the record
        """
        s3_path = self._build_s3_path(
            locator=locator, md5=False, prefix="opcit"
        )
        if not quiet:
            print(
                f"[[bold yellow]Fetching[/bold yellow]] {s3_path} from {self._bucket}"
            )

        json_list = json.loads(
            self._aws_connector.s3_obj_to_str(
                bucket=self._bucket, s3_path=s3_path, raise_on_fail=True
            )
        )

        return json_list

    def write_main_record(self, record: str, endpoint: str) -> None:
        print(f"Storing: {endpoint}")

        self._aws_connector.push_json_to_s3(
            json_obj=record, bucket=self._bucket, path=endpoint
        )

    def write_shelob_record(self, record: str, endpoint: str) -> None:
        print(f"Storing: {endpoint}")

        self._shelob_aws_connector.push_json_to_s3(
            json_obj=record, bucket=self._shelob_bucket, path=endpoint
        )

    def list_records(self, doi=False):
        if not doi:
            return self._aws_connector.list_prefix(prefix="opcit/works/")

        dois = []

        file_list = self._aws_connector.list_prefix(prefix="opcit/works/")

        for file in file_list:
            # get the s3 object
            s3_obj = self._aws_connector.s3_obj_to_str(
                bucket=self._bucket,
                s3_path=f"opcit/works/{file}",
                raise_on_fail=False,
            )

            dois.append(json.loads(s3_obj)["doi"])

        return dois

    def _view_record(self, entry):
        return self.get_history_record(locator=entry)

    def _get_history(self, entry):
        history = self.get_history_record(locator=entry)

        return history

    @staticmethod
    def sort_history(history):
        return sorted(history, key=operator.itemgetter("datestamp"))

    @staticmethod
    def filter_history(history, url):
        return [record for record in history if record["url"] == url]

    def rebase_history(self, first, last, penultimate, ante_penultimate):
        """
        Rebase the history taking the last totally good entry as the baseline
        :param first: the first entry in the history
        :param last: the last entry in the history
        :param penultimate: the penultimate entry in the history
        :param ante_penultimate: the ante-penultimate entry in the history
        :return: the rebased history
        """
        # determine the current policy based on the first entry
        use_doi, use_status, use_title = self.determine_policy(first)

        switch = False
        switch_object = None
        switch_reason = ""

        # if the status switches to 200 AND a title or DOI appears, switch
        if not use_status and last["status_code"] == 200:
            if last["has_title"] is True or last["has_doi"] is True:
                switch = True
                switch_object = last
                switch_reason += (
                    "Last entry saw appearance of title or DOI and "
                    "changed status to 200. "
                )
        elif not use_status and penultimate["status_code"] == 200:
            if penultimate is not None and (
                penultimate["has_title"] is True
                or penultimate["has_doi"] is True
            ):
                switch = True
                switch_object = penultimate
                switch_reason += (
                    "Penultimate entry saw appearance of title or "
                    "DOI and changed status to 200. "
                )
        elif not use_status and ante_penultimate["status_code"] == 200:
            if ante_penultimate is not None and (
                ante_penultimate["has_title"] is True
                or ante_penultimate["has_doi"] is True
            ):
                switch = True
                switch_object = ante_penultimate
                switch_reason += (
                    "Antepenultimate entry saw appearance of title or DOI "
                    "and changed status to 200. "
                )

        # if the title suddenly appears
        if not use_title and last["has_title"] is True:
            switch = True
            switch_object = last
            switch_reason += "Last entry saw reappearance of title. "
        elif not use_title and penultimate["has_title"] is True:
            switch = True
            switch_object = penultimate
            switch_reason += "Penultimate entry saw reappearance of title. "
        elif not use_title and ante_penultimate["has_title"] is True:
            switch = True
            switch_object = ante_penultimate
            switch_reason += "Antepenultimate entry saw reappearance of title. "

        # if the DOI suddenly appears
        if not use_doi and last["has_doi"] is True:
            switch = True
            switch_object = last
            switch_reason += "Last entry saw reappearance of DOI."
        elif not use_doi and penultimate["has_doi"] is True:
            switch = True
            switch_object = penultimate
            switch_reason += "Penultimate entry saw reappearance of DOI."
        elif not use_doi and ante_penultimate["has_doi"] is True:
            switch = True
            switch_object = ante_penultimate
            switch_reason += "Antepenultimate entry saw reappearance of DOI."

        if switch:
            first = switch_object
            last = None
            penultimate = None
            ante_penultimate = None
            print(f"History rebased: {switch_reason}")

        return first, last, penultimate, ante_penultimate

    @staticmethod
    def _merge_endpoint_records(old_record: list, new_record: dict) -> list:
        """
        Merge two endpoint records
        :param old_record: the old record
        :param new_record: the new record
        :return: the merged record
        """
        # if there are no existing records, return the new record wrapped in
        # a list
        if len(old_record) == 0:
            return [new_record]

        # sort the list by date so the most recent is last
        sorted_list: list = sorted(
            old_record, key=operator.itemgetter("datestamp")
        )
        last_record: dict = sorted_list[-1]

        # if the last record is stable, add the new record to the list
        # and remove the stable tag if the most recent item is now stable
        if "stable" in last_record["tags"]:
            # if the last record is stable, add the new record to the list
            sorted_list.append(new_record)

            # if this is a stable entry, reassign the stable tag
            if "stable" in new_record["tags"]:
                last_record["tags"].remove("stable")
        else:
            sorted_list = Shelob._find_last_stable_and_update(
                new_record, sorted_list
            )

        return sorted_list

    @staticmethod
    def _find_last_stable_and_update(new_record, sorted_list):
        # the last record is not stable, so iterate backwards through
        # the list until we find a stable record
        distance: int = 0
        reverse_list: list = list(reversed(sorted_list))

        for record in reverse_list:
            distance += 1

            if "stable" in record["tags"]:
                # if this is a stable entry, reassign the stable tag
                if "stable" in new_record["tags"]:
                    record["tags"].remove("stable")
                break

        # if the distance is >=4, then we need to raise an alert
        if distance >= 4:
            print(
                f"[[bold red]Alert[/bold red]] {distance} snapshots since "
                f"stable"
            )

        sorted_list = list(reversed(list(reverse_list)))
        sorted_list.append(new_record)

        return sorted_list

    def has_died(self, history, url, doi=None):
        if len(history) < 3:
            # insufficient data
            print(
                "Fewer than three entries in history. Unable to determine "
                "state."
            )
            return False

        new_history = self.filter_history(history, url)
        sorted_history = self.sort_history(new_history)

        print(
            f"[[bold green]Found history[/bold green]] "
            f"{len(sorted_history)} entries"
        )

        try:
            first = sorted_history[0]
            last = sorted_history[-1]
            penultimate = sorted_history[-2]
            ante_penultimate = sorted_history[-3]

            first, last, penultimate, ante_penultimate = self.rebase_history(
                first, last, penultimate, ante_penultimate
            )

            if (
                last is None
                and penultimate is None
                and ante_penultimate is None
            ):
                print("Rebased history")

                history = list[first]

                if doi:
                    self.write_shelob_record(
                        record=json.dumps(history),
                        endpoint=self._build_s3_path(
                            locator=doi,
                        ),
                    )

                raise IndexError
        except IndexError:
            print("Insufficient data to judge or history was rebased.")
            return False

        # check the state of the last entry for fast exit
        if (
            last["status_code"] == 200
            and last["has_title"] is True
            and last["has_doi"] is True
        ):
            print("Last entry is all good.")
            return False

        # check for the fields to use based on the first entry
        use_doi, use_status, use_title = self.determine_policy(first)

        # if all fields to use are False, then we can't tell whether the
        # resource has died
        if not use_title and not use_doi and not use_status:
            print(
                "[[bold yellow]Result[/bold yellow]] First entry had no "
                "good data fields to use"
            )
            return False

        if use_status:
            # if the status code is not 200 in the last three, then the
            # resource has died
            if (
                last["status_code"] != 200
                and penultimate["status_code"] != 200
                and ante_penultimate["status_code"] != 200
            ):
                print(
                    "[[bold red]Result[/bold red]] Three failed status "
                    "checks in a row. Resource has died"
                )
                return True

        if use_title:
            # if the title is not found in the last three, then the resource
            # has died
            if (
                ("has_title" in last and last["has_title"] is False)
                and (
                    "has_title" in penultimate
                    and penultimate["has_title"] is False
                )
                and (
                    "has_title" in ante_penultimate
                    and ante_penultimate["has_title"] is False
                )
            ):
                print(
                    "[[bold red]Result[/bold red]] Three failed title "
                    "checks in a row. Resource has died"
                )
                return True

        if use_doi:
            # if the DOI is not found in the last three, then the resource
            # has died
            if (
                last["has_doi"] is False
                and penultimate["has_doi"] is False
                and ante_penultimate["has_doi"] is False
            ):
                print(
                    "[[bold red]Result[/bold red]] Three failed DOI "
                    "checks in a row. Resource has died"
                )
                return True

        print("[[bold green]Result[/bold green]] Resource is alive")
        return False

    def determine_if_died(self, entry):
        # first, get the entry from S3 and extract the DOI
        record: dict = self.get_metadata_record(locator=entry)

        if record is None or len(record) == 0:
            print(f"[[bold red]No record found[/bold red]] {entry}")
            return None

        doi = record["doi"]
        title = record["title"]
        print(f"[[bold green]Snapshotting[/bold green]] {doi}")

        # fetch any past lookups for this entry
        json_entry = self._get_history(entry=doi)

        if type(json_entry) is str:
            history = json.loads(json_entry)
        else:
            history = []

        resource_statuses = {}

        for resource in record["resources"]:
            result = self.snapshot(
                url=resource, doi=doi, title=title, record=history
            )

            if len(history) == 0:
                history = [result]
            else:
                history.append(result)

            resource_statuses[resource] = self.has_died(
                history=history, url=resource, doi=doi
            )

        # write the record back to S3
        self.write_shelob_record(
            record=json.dumps(history),
            endpoint=self._build_s3_path(
                locator=doi,
            ),
        )

        return resource_statuses

    @staticmethod
    def determine_policy(first):
        try:
            use_title = first["has_title"]
        except KeyError:
            use_title = False
        try:
            use_doi = first["has_doi"]
        except KeyError:
            use_doi = False
        try:
            use_status = first["status_code"] == 200
        except KeyError:
            use_status = False
        return use_doi, use_status, use_title

    def is_multiple_resolution_enabled(self, doi, quiet=False):
        """
        Check if multiple resolution is enabled
        :param doi: the DOI to check
        :param quiet: suppress output
        :return: True if multiple resolution is enabled, False otherwise
        """
        record = self.get_current_status(locator=doi, quiet=quiet)

        if record is None or len(record) == 0:
            if not quiet:
                print(
                    f"[[bold red]No multiple resolution record found[/bold red]] "
                    f"{doi}"
                )
            return False
        else:
            return True

    @staticmethod
    def render_template(doi, email, archival_outputs):
        """
        Render the deposit template for a resource-only deposit
        :param doi: the DOI
        :param email: the email
        :param archival_outputs: the list of archival outputs
        :return: the rendered template
        """
        deposit_template = (
            '<?xml version="1.0" encoding="UTF-8"?>\n'
            '<doi_batch version="4.4.2" '
            'xmlns="http://www.crossref.org/doi_resources_schema/4.4.2" '
            'xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" '
            'xsi:schemaLocation="http://www.crossref.org/doi_resources_schema/4.4.2 http://www.crossref.org/schema/deposit/doi_resources4.4.2.xsd">\n'
            "<head>"
            "<doi_batch_id>{{ batch_id }}</doi_batch_id>"
            "<depositor>"
            "<depositor_name>Op Cit</depositor_name>"
            "<email_address>{{ email }}</email_address>"
            "</depositor>"
            "</head>"
            "<body>"
            "<doi_resources>"
            "<doi>{{ doi }}</doi>"
            '<collection property="list-based">'
        )

        counter = 0
        for output in archival_outputs:
            if output:
                counter = counter + 1
                deposit_template += (
                    '<item label="SECONDARY_' + str(counter) + '">'
                    "<resource>" + output + "</resource>"
                    "</item>"
                )

        deposit_template += (
            "</collection>" "</doi_resources>" "</body>" "</doi_batch>"
        )

        deposit_template = Template(deposit_template)

        deposit_context = {
            "doi": doi,
            "email": email,
            "batch_id": str(uuid.uuid4()),
        }

        return deposit_template.render(deposit_context)

    def update_deposit(
        self, doi, entries, template_xml=None, write_enabled=True
    ):
        url = "https://doi.crossref.org/servlet/"

        client = httpx.Client(base_url=url, timeout=30)

        url = httpx.URL(path="deposit")

        if not template_xml:
            xml = self.render_template(
                doi=doi,
                email="meve@crossref.org",
                archival_outputs=entries["archive_resources"],
            )
        else:
            xml = template_xml

        uf = {"mdFile": ("secondary_upload.xml", xml)}

        data = {
            "login_id": "opcit",
            "login_passwd": os.environ.get("OP_CIT_PASSWORD", ""),
            "operation": "doDOICitUpload",
        }

        req = client.build_request(
            method="POST",
            url=url,
            data=data,
            files=uf,
        )

        if write_enabled:
            self._enable_mr(locator=doi, status="Enabled MR")
        else:
            self._aws_connector.s3_client.delete_object(
                Bucket=self._shelob_bucket,
                Key=self._build_s3_path(locator=doi, prefix="status"),
            )

        return client.send(req, stream=True)

    def list_prefixes(self, filenames=False):
        """
        List the prefixes that have registered for Op Cit
        :param filenames: whether to return the filenames. If True return
            filenames of prefix containers, if False return the list of prefixes
        :return: the list of prefixes
        """
        if filenames:
            return self._aws_connector.list_prefix(prefix="opcit/prefixes/")

        return_list = []

        for prefix in self._aws_connector.list_prefix(prefix="opcit/prefixes/"):
            file_contents = json.loads(
                self._aws_connector.s3_obj_to_str(
                    bucket=self._bucket, s3_path=f"opcit/prefixes/{prefix}"
                )
            )

            return_list.append(file_contents["prefix"])

        return return_list

    def prefixes_status(self, filenames, polite_email=None, eligible=False):
        for prefix in filenames:
            file_contents = json.loads(
                self._aws_connector.s3_obj_to_str(
                    bucket=self._bucket, s3_path=f"opcit/prefixes/{prefix}"
                )
            )

            prefix = file_contents["prefix"]

            prefix_works = self._get_prefix_dois_from_api(
                prefix=prefix, polite_email=polite_email, eligible=eligible
            )

            return prefix_works

    def _get_prefix_dois_from_api(
        self, prefix, polite_email=None, eligible=False
    ):
        """
        Get a list of DOIs and Op Cit statuses for a specific prefix
        :param prefix: the prefix
        :param polite_email: the email to use for the Polite API
        :return: the list of DOIs and statuses
        """

        processed = []

        response = (
            self.fetch(
                f"{self.crossref_api_endpoint}/prefixes/{prefix}"
                f"/works?rows=1000"
            )
            if not polite_email
            else self.fetch(
                f"{self.crossref_api_endpoint}/prefixes/{prefix}"
                f"/works?rows=1000&mailto={polite_email}"
            )
        )

        responses = []

        response_json = response.json()

        total_results = int(response_json["message"]["total-results"])

        self._process_dois(
            prefix,
            response_json,
            responses,
            eligible=eligible,
            processed=processed,
        )

        for i in range(0, total_results, 1000):
            response = (
                self.fetch(
                    f"{self.crossref_api_endpoint}/prefixes/{prefix}"
                    f"/works?rows=1000&offset={i}"
                )
                if not polite_email
                else self.fetch(
                    f"{self.crossref_api_endpoint}/prefixes/{prefix}"
                    f"/works?rows=1000&offset={i}&mailto={polite_email}"
                )
            )

            response_json = response.json()

            self._process_dois(
                prefix,
                response_json,
                responses,
                eligible=eligible,
                processed=processed,
            )

        return responses

    def _process_dois(
        self, prefix, response_json, responses, eligible=False, processed=[]
    ):
        """
        Iterate over an API response and append the Op Cit status data to the
        output
        :param prefix: the prefix in question
        :param response_json: the JSON response from the API
        :param responses: the list of responses to append to
        :param eligible: whether to only return eligible DOIs
        :param processed: the list of already processed DOIs
        :return: None
        """
        for work in response_json["message"]["items"]:
            doi = work["DOI"]
            try:
                title = work["title"][0]
            except KeyError:
                title = ""

            vor, opcit, pdf = self.opcit_status(
                doi=doi, response_json=work, quiet=True
            )

            if (
                (eligible and vor and not opcit) or (not eligible)
            ) and doi not in processed:
                processed.append(doi)

                responses.append(
                    {
                        "doi": doi,
                        "title": title,
                        "prefix": prefix,
                        "vor": vor,
                        "opcit_enabled": opcit,
                        "pdf": pdf,
                    }
                )

    def _get_work_from_api(self, doi):
        response = self.fetch(f"{self.crossref_api_endpoint}/works/{doi}")

        return response.json()["message"]

    def opcit_status(self, doi, response_json, quiet=False):
        """
        Get basic Op Cit status on a DOI
        :param doi: the DOI to get the status of
        :param response_json: the response from the API
        :param quiet: suppress output
        :return: the response
        """

        vor = False
        pdf = None

        if "link" in response_json:
            for link in response_json["link"]:
                if (
                    link["content-version"] == "vor"
                    and link["intended-application"] == "text-mining"
                    and link["content-type"] == "application/pdf"
                ):
                    pdf = link["URL"]
                    vor = True
                elif (
                    link["content-version"] == "vor"
                    and link["intended-application"] == "similarity-checking"
                    and link["content-type"] == "application/pdf"
                ):
                    pdf = link["URL"]
                    vor = True
        else:
            vor = False
            pdf = None

        try:
            md5_doi = Annotator.doi_to_md5(doi)
            entries = self.get_metadata_record(locator=md5_doi, quiet=quiet)
            opcit_record = True
        except S3ObjException:
            if not quiet:
                print(
                    f"[[bold red]Failed[/bold red]] {doi} is not an Op Cit record"
                )
            opcit_record = False

        return vor, opcit_record, pdf

    def check_new_doi(self, doi, quiet=False):
        """
        Ingest a new DOI
        :param doi: the DOI to ingest
        :param quiet: suppress output
        :return: the response
        """
        vor = False

        response = self.fetch(f"{self.crossref_api_endpoint}/works/{doi}")

        response = response.json()

        vor, opcit_record, pdf = self.opcit_status(
            doi=doi, response_json=response["message"], quiet=quiet
        )

        title = response["message"]["title"]

        result = self.snapshot(
            url=response["message"]["resource"]["primary"]["URL"],
            doi=doi,
            title=title,
            record={},
        )

        end_message = f"[[bold green]{doi}[/bold green]]"

        end_message = self._add_message(
            end_message, conditional=vor, positive="VOR", negative="No VOR"
        )

        end_message = self._add_message(
            end_message,
            conditional=opcit_record,
            positive="OpCit Record",
            negative="No OpCit Record",
        )

        end_message = self._add_message(
            end_message,
            conditional=result["status_code"] == 200,
            positive="200",
            negative=result["status_code"],
        )

        end_message = self._add_message(
            end_message,
            conditional=result["has_title"],
            positive="Title Found",
            negative="No Title Found",
        )

        end_message = self._add_message(
            end_message,
            conditional=result["has_doi"],
            positive="DOI Found",
            negative="No DOI Found",
        )

        is_mr = self.is_multiple_resolution_enabled(doi=doi, quiet=quiet)

        end_message = self._add_message(
            end_message,
            conditional=is_mr,
            positive="Multiple Resolution",
            negative="No Multiple Resolution",
        )

        print(end_message)

        return vor, opcit_record, result, is_mr

    @staticmethod
    def _add_message(end_message, conditional, positive, negative):
        end_message += (
            f" [[bold green]{positive}[/bold green]]"
            if conditional
            else f" [[bold red]{negative}[/bold red]]"
        )
        return end_message

    def activate_doi(self, doi, username, password, email):
        cred = credcheck.cred.Credential(
            username=username, password=password, doi=doi
        )

        if not cred.is_authenticated() or not cred.is_authorised():
            print(f"[[bold red]Auth[/bold red]] failed")
            return False

        # create an md5 of the prefix
        md5_doi = clannotation.annotator.Annotator.doi_to_md5(doi)

        # fetch the title from the Crossref REST API
        rest_api_response = httpx.get(
            f"https://api.crossref.org/works/{doi}"
        ).read()

        try:
            rest_api_response = json.loads(rest_api_response)

            title = rest_api_response["message"]["title"][0]
            resource = rest_api_response["message"]["resource"]["primary"][
                "URL"
            ]
            license = (
                "creativecommons"
                in rest_api_response["message"]["license"][0]["URL"]
                if "license" in rest_api_response["message"]
                else False
            )

            if "link" in rest_api_response["message"]:
                for link in rest_api_response["message"]["link"]:
                    if (
                        link["content-version"] == "vor"
                        and link["intended-application"] == "text-mining"
                    ):
                        pdf = link["URL"]
            else:
                pdf = None

        except Exception:
            print(f"[[bold red]REST API[/bold red]] failed to retrieve fields")
            return False

        if not license:
            print(
                f"[[bold red]CC License[/bold red]] not found for {doi}. "
                "This item will not be archived."
            )
            return False

        if not link:
            print(
                f"[[bold red]PDF[/bold red]] not found for {doi}. "
                "This item will not be archived."
            )
            return False

        outputs = []
        new_outputs = []

        outputs.append(resource)

        try:
            file_object = self.fetch_url(url=pdf)
        except Exception as e:
            message = (
                f"[[bold red]Error[/bold red]] fetching {pdf}: {e}. "
                "This item will not be archived."
            )
            print(message)
            file_object = None

        output = None

        if file_object:
            # deposit in archives
            deposit_systems = {"Internet Archive": "ia"}
            for (
                depositor_name,
                depositor_module,
            ) in deposit_systems.items():
                warnings, output = self.deposit(
                    depositor_name=depositor_name,
                    depositor_module=depositor_module,
                    file_object=file_object,
                    doi=doi,
                    title=title,
                )

                new_outputs.append(output)

        # build the DOI to MD5 and push the JSON to S3
        md5_doi = clannotation.annotator.Annotator.doi_to_md5(doi)
        json_response = self.build_resource(
            doi=doi,
            title=title,
            outputs=outputs,
            archive_outputs=new_outputs,
            license="CC",
        )

        # write the entry to S3
        self._aws_connector.push_json_to_s3(
            json_obj=json_response,
            path=f"opcit/works/{md5_doi}.json",
            bucket=self._bucket,
        )

    @staticmethod
    def fetch_url(url) -> bytes:
        """
        Fetch a remote URL
        :param url: the URL to fetch
        :return: the response content
        """
        with httpx.Client() as client:
            response = client.get(url)

            return response.content

    @staticmethod
    def deposit(
        depositor_name,
        depositor_module,
        file_object,
        doi,
        title,
    ) -> tuple[list, str]:
        """
        Deposit a file in an archive
        :param doi: the DOI
        :param depositor_name: the name
        :param depositor_module: depositor module string
        :param file_object: the file object to deposit
        :param title: the title
        :return: a list of warnings and an output location string
        """
        print(f"Loading module {depositor_name}")
        process_module = import_module(f"{depositor_module}")

        # build a metadata dictionary
        metadata = {
            "title": title,
            "date": datetime.datetime.now().isoformat(),
            "doi": doi,
        }

        warnings = []

        archive = process_module.Archive(print)
        warnings, output = archive.deposit(
            file_object=file_object,
            warnings=warnings,
            doi=doi,
            metadata=metadata,
        )

        print(
            f"[[green bold]Deposited[/green bold]] {doi} to {depositor_name} "
            f"at {output}"
        )

        return warnings, output

    @staticmethod
    def build_resource(doi, title, outputs, archive_outputs, license):
        """
        Build a resource dictionary
        :param doi: the DOI
        :param title: the title
        :param outputs: the list of original outputs
        :param archive_outputs: the list of archive outputs
        :param license: the license
        :return: a dictionary
        """
        return {
            "doi": doi,
            "title": title,
            "license": license,
            "resources": outputs,
            "archive_resources": archive_outputs,
            "date": datetime.date.today().isoformat(),
        }

    def deregister_doi(self, doi):
        md5_doi = Annotator.doi_to_md5(doi)

        print(f"[[green bold]Removing[/green bold]] opcit/works/{md5_doi}.json")

        self._aws_connector.s3_client.delete_object(
            Bucket=self._bucket, Key=f"opcit/works/{md5_doi}.json"
        )

        print(f"[[green bold]Deregistering[/green bold]] Multiple Resolution")
        template_xml = self.blank_template(doi=doi, email="labs@crossref.org")
        self.update_deposit(
            doi=doi, entries={}, template_xml=template_xml, write_enabled=False
        )

    def blank_template(self, doi, email):
        """
        Render the deposit template for a resource-only deposit
        :param request: the request object
        :param doi: the DOI
        :param email: the email
        :return: the rendered template
        """
        deposit_template = (
            '<?xml version="1.0" encoding="UTF-8"?>\n'
            '<doi_batch version="4.4.2" '
            'xmlns="http://www.crossref.org/doi_resources_schema/4.4.2" '
            'xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" '
            'xsi:schemaLocation="http://www.crossref.org/doi_resources_schema/4.4.2 http://www.crossref.org/schema/deposit/doi_resources4.4.2.xsd">\n'
            "<head>"
            "<doi_batch_id>{{ batch_id }}</doi_batch_id>"
            "<depositor>"
            "<depositor_name>Op Cit</depositor_name>"
            "<email_address>{{ email }}</email_address>"
            "</depositor>"
            "</head>"
            "<body>"
            "<doi_resources>"
            "<doi>{{ doi }}</doi>"
            '<collection property="list-based">'
            "</collection>"
            "</doi_resources>"
            "</body>"
            "</doi_batch>"
        )

        deposit_template = Template(deposit_template)

        deposit_context = {
            "doi": doi,
            "email": email,
            "batch_id": str(uuid.uuid4()),
        }

        return deposit_template.render(deposit_context)

    def activate_prefix(self, prefix_value, username, password, email):
        # first, fetch a DOI
        base_api_url = f"https://api.crossref.org/prefixes/{prefix_value}/works"
        response = json.loads(httpx.get(base_api_url).read())

        items = response["message"].get("items", [])

        if len(items) == 0:
            print("The DOI prefix must have at least one active entry")
            return

        doi_to_verify = items[0].get("DOI", None)

        cred = credcheck.cred.Credential(
            username=username, password=password, doi=doi_to_verify
        )

        if not cred.is_authenticated():
            print("The supplied credentials are invalid")
            return
        elif not cred.is_authorised():
            print("The supplied credentials are invalid for this prefix")
            return

        # create an md5 of the prefix
        md5_prefix = clannotation.annotator.Annotator.doi_to_md5(prefix_value)

        json_object = {
            "prefix": prefix_value,
            "user": username,
            "email": email,
            "date": datetime.date.today().isoformat(),
        }

        # write the entry to S3
        settings.Settings.fetch_secrets()

        settings.Settings.AWS_CONNECTOR.push_json_to_s3(
            json_obj=json_object,
            path=f"opcit/prefixes/{md5_prefix}.json",
            bucket=settings.Settings.BUCKET,
        )

    def deregister_prefix(self, prefix_value):
        md5_prefix = clannotation.annotator.Annotator.doi_to_md5(prefix_value)

        print(
            f"[[green bold]Removing[/green bold]] opcit/prefixes/{md5_prefix}.json"
        )

        self._aws_connector.s3_client.delete_object(
            Bucket=self._bucket, Key=f"opcit/prefixes/{md5_prefix}.json"
        )
