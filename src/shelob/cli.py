import json

import clannotation.annotator
import typer
from claws.aws_utils import S3ObjException
from rich import print
from typing_extensions import Annotated

from shelob import Shelob

app = typer.Typer()


@app.command()
def list_entries(
    doi: Annotated[
        bool,
        typer.Option("--dois", help="Show DOIs instead of filenames (slow)"),
    ] = False,
):
    """
    Give a list of filenames for DOIs that are monitored by Op Cit
    """
    entries = Shelob().list_records(doi=doi)

    print(entries)


@app.command()
def check_history(
    doi: Annotated[str, typer.Argument(help="Determine the status of a DOI")]
):
    """
    Determine the status of a DOI by looking at its history
    """

    # MD5 the DOI
    md5_doi = clannotation.annotator.Annotator.doi_to_md5(doi)

    shelob = Shelob()

    # first, fetch the list of resources from the S3 file
    entries = shelob.get_metadata_record(locator=md5_doi)
    record = shelob.get_history_record(locator=doi)
    try:
        history = json.loads(record)
    except TypeError:
        history = record

    print(history)

    if entries is None:
        print("No access status has yet been ascertained for this work")
        return

    resource_list = entries["resources"]

    for resource in resource_list:
        print(f"[[bold green]Evaluating resource[/bold green]] {resource}")
        shelob.has_died(history=history, url=resource)


@app.command()
def status_check_and_update(
    doi: Annotated[str, typer.Argument(help="Determine the status of a DOI")],
    force: Annotated[
        bool, typer.Option("--force", help="Simulate the resource being down")
    ] = False,
    no_mr_check: Annotated[
        bool,
        typer.Option(
            "--no-mr-check",
            help="Do not check existing multiple resolution status",
        ),
    ] = False,
):
    """
    Check a resource's status and activate multiple resolution if it's dead
    """
    shelob = Shelob()

    if no_mr_check or not shelob.is_multiple_resolution_enabled(doi=doi):
        md5_doi = clannotation.annotator.Annotator.doi_to_md5(doi)

        # first, fetch the list of resources from the S3 file
        try:
            entries = shelob.get_metadata_record(locator=md5_doi)
        except S3ObjException:
            print(
                f"[[bold red]Failed[/bold red]] {doi} is not an Op Cit record"
            )
            return

        if not force:
            history_record = shelob.get_history_record(locator=doi)

            try:
                history = json.loads(history_record)
            except TypeError:
                history = history_record

            print(history)

            if entries is None or len(entries) == 0:
                print(
                    "[[bold red]Failed[/bold red]] No access status has "
                    "yet been ascertained for this work"
                )
                return

            resource_list = entries["resources"]

            has_died = []

            for resource in resource_list:
                print(
                    f"[[bold yellow]Evaluating resource[/bold yellow]] "
                    f"{resource}"
                )
                has_died.append(shelob.has_died(history=history, url=resource))

            # update the record and history
            if True in has_died:
                print(
                    f"[[bold red]The resource has died[/bold red]] "
                    f"Activating multiple resolution."
                )
                submit_to_deposit(doi, entries, shelob)
            else:
                print(
                    f"[[bold green]Alive[/bold green]] The resource is alive."
                )
        else:
            print(
                f"[[bold red]Simulating[/bold red]] "
                f"Resource has died. Activating multiple resolution."
            )

            submit_to_deposit(doi, entries, shelob)
    else:
        print(
            f"[[bold green]Multiple resolution[/bold green]] "
            f"Already enabled for this DOI"
        )


def submit_to_deposit(doi, entries, shelob):
    status = shelob.update_deposit(doi=doi, entries=entries)
    if status.status_code == 200:
        print(f"[[bold green]Submitted[/bold green]] XML submitted.")
    else:
        print(f"[[bold red]Failure[/bold red]] XML submission failure.")


@app.command()
def update_history(
    doi: Annotated[str, typer.Argument(help="The expected DOI")],
):
    """
    Update and store a DOI history record
    """
    # MD5 the DOI
    md5_doi = clannotation.annotator.Annotator.doi_to_md5(doi)

    shelob = Shelob()

    # first, fetch the list of resources from the S3 file
    entries = shelob.get_metadata_record(locator=md5_doi)
    record = shelob.get_history_record(locator=doi)
    try:
        history = json.loads(record)
    except TypeError:
        history = record

    shelob.determine_if_died(entry=md5_doi)


@app.command()
def list_prefixes(
    filenames: Annotated[
        bool, typer.Option("--filenames", help="Return only filenames")
    ] = False,
):
    """
    List the prefixes that are enabled for Op Cit
    """

    print(Shelob().list_prefixes(filenames=filenames))


@app.command()
def prefix_status(
    polite_email: Annotated[
        str, typer.Argument(help="The email to use for polite etiquette")
    ],
    eligible: Annotated[
        bool, typer.Option("--eligible", help="Show only eligible DOIs")
    ] = False,
):
    """
    Show a list of DOIs and their Op Cit statuses under all registered prefixes
    """
    shelob = Shelob()

    results = shelob.prefixes_status(
        shelob.list_prefixes(filenames=True),
        polite_email=polite_email,
        eligible=eligible,
    )

    print(results)
    print(f"Found {len(results)} entries in this prefix")


@app.command()
def doi_status(
    doi: Annotated[str, typer.Argument(help="The DOI to check")],
):
    """
    Check the status of a DOI (e.g. whether Op Cit enabled, whether we have a
    VOR, etc.)
    """
    shelob = Shelob()

    shelob.check_new_doi(doi, quiet=True)


@app.command()
def register_new_doi(
    doi: Annotated[str, typer.Argument(help="The DOI")],
    username: Annotated[str, typer.Argument(help="The Crossref username")],
    password: Annotated[str, typer.Argument(help="The Crossref password")],
    email: Annotated[str, typer.Argument(help="The email for deposit")],
):
    """
    Register a new DOI for Op Cit monitoring
    """
    shelob = Shelob()

    shelob.activate_doi(doi, username, password, email)


@app.command()
def register_new_prefix(
    prefix: Annotated[str, typer.Argument(help="The prefix to activate")],
    username: Annotated[str, typer.Argument(help="The Crossref username")],
    password: Annotated[str, typer.Argument(help="The Crossref password")],
    email: Annotated[str, typer.Argument(help="The email for deposit")],
):
    """
    Register a new prefix for Op Cit monitoring
    """
    shelob = Shelob()

    shelob.activate_prefix(prefix, username, password, email)


@app.command()
def deregister_doi(
    doi: Annotated[str, typer.Argument(help="The DOI")],
):
    """
    Deregister a DOI from Op Cit monitoring
    """
    shelob = Shelob()

    shelob.deregister_doi(doi)


@app.command()
def deregister_prefix(
    prefix: Annotated[str, typer.Argument(help="The prefix")],
):
    """
    Deregister a prefix from Op Cit monitoring
    """
    shelob = Shelob()

    shelob.deregister_prefix(prefix)


if __name__ == "__main__":
    app()
