# Shelob: The Content Drift Spider
Shelob is a spider that detects content drift on artifacts preserved with Project Op Cit. At present, this is a work-in-progress, request for comment repo and the project is not yet built.

![license](https://img.shields.io/gitlab/license/crossref/labs/shelob) ![activity](https://img.shields.io/gitlab/last-commit/crossref/labs/shelob)

![AWS](https://img.shields.io/badge/AWS-%23FF9900.svg?style=for-the-badge&logo=amazon-aws&logoColor=white) ![Linux](https://img.shields.io/badge/Linux-FCC624?style=for-the-badge&logo=linux&logoColor=black) ![Python](https://img.shields.io/badge/python-3670A0?style=for-the-badge&logo=python&logoColor=ffdd54)

Shelob has a number of proposed functions:

1. To store a page's† initial state and verify initial metadata (e.g. DOI on a landing page)
2. To create a periodic scheduler to know when to revisit pages
3. To detect when the _content_ has changed rather than just the binary bits (semantic drift)
4. To alert various parties, including Op Cit, when we believe content has been changed

† Op Cit, for now, is working only with PDFs

Initial plans:

* We will use S3 to handle data storage
* Scheduling will be handled by writing to MD5 entries for specific dates. When the scheduler runs, it MD5s today's date to retrieve the list of endpoints to check
* Records of endpoints are also stored in MD5ed object-named entries for each endpoint
* Endpoint records will store as a JSON list, for each visit:
  * Visit datetime
  * File hash string
  * DOI present boolean
  * Title present boolean
* On content drift (file hash change, DOI present change to False, title present change to False), Shelob will schedule a follow-up visit a week later. On second fail, Shelob will again schedule a follow-up another week later. On a third fail, Shelob will raise the alarm.

## Usage
Shelob currently contains a set of scripts that allow you to test functionality against a sample set of DOIs.

    Usage: cli.py [OPTIONS] COMMAND [ARGS]...                                                                                                                           
                                                                                                                                                                         
    ╭─ Options ─────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────╮
    │ --install-completion        [bash|zsh|fish|powershell|pwsh]  Install completion for the specified shell. [default: None]                                          │
    │ --show-completion           [bash|zsh|fish|powershell|pwsh]  Show completion for the specified shell, to copy it or customize the installation. [default: None]   │
    │ --help                                                       Show this message and exit.                                                                          │
    ╰───────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────╯
    ╭─ Commands ────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────╮
    │ check-history                                       Determine the status of a DOI by looking at its history                                                       │
    │ deregister-doi                                      Deregister a DOI from Op Cit monitoring                                                                       │
    │ deregister-prefix                                   Deregister a prefix from Op Cit monitoring                                                                    │
    │ doi-status                                          Check the status of a DOI (e.g. whether Op Cit enabled, whether we have a VOR, etc.)                          │
    │ list-entries                                        Give a list of filenames for DOIs that are monitored by Op Cit                                                │
    │ list-prefixes                                       List the prefixes that are enabled for Op Cit                                                                 │
    │ prefix-status                                       Show a list of DOIs and their Op Cit statuses under all registered prefixes                                   │
    │ register-new-doi                                    Register a new DOI for Op Cit monitoring                                                                      │
    │ register-new-prefix                                 Register a new prefix for Op Cit monitoring                                                                   │
    │ status-check-and-update                             Check a resource's status and activate multiple resolution if it's dead                                       │
    │ update-history                                      Update and store a DOI history record                                                                         │
    ╰───────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────╯

### Command Sequence: Preserve a DOI

1. register-new-doi (register the new DOI) [one-time]
2. doi-status (check the DOI is registered) [one-time]
3. check-history (check the blank history) [one-time]
4. update-history (create a history entry) [loop to check status]
5. status-check-and-update (check the entry is still alive) [loop to check status]


### Command Sequence: Preserve a Prefix

1. register-new-prefix (register the new prefix) [one-time]
2. list-prefixes (check the prefix is registered) [one-time]
3. prefix-status --eligible (get a list of DOIs that are Op Cit eligible from the prefix) [one-time]
4. register-new-doi (register the new DOI) [one-time/loop if new DOIs are present]
5. update-history (create a history entry) [loop to check status]
6. status-check-and-update (check the entry is still alive) [loop to check status]

### Command Sequence: Spider and Check All Entries

1. list-entries (get a list of all DOI entries) [loop to check status]
2. update-history (create a history entry) [loop to check status]
3. status-check-and-update (check the entry is still alive) [loop to check status]

## Baseline and History Profiling
Shelob will store a baseline profile of each landing page and a history of changes. This will allow us to detect when a landing page has changed and to alert Op Cit when we believe content has been changed.

There are several initial baseline values that Shelob checks. On first visit, the page status is profiled to determine whether it is an HTTP 200 response, whether we can find the title on the landing page, and whether we can find a DOI in the recommended format on the landing page. If any of these values is False or not a 200 response on the first visit, then each feature respectively will be excluded from the profiling exercise. That is, if when we first visit the status code is 404, future status checks will not be used to check whether a page has died. Likewise, when we first visit, if we cannot extract the title or DOI then we will not use these as baseline measures.

A trigger event/alert is tripped when a measure that was successful in the baseline reading (status code, title present, DOI present) changes status for three readings in a row. So, a historical sequence of 200, 404, 200, 404 would not count as a failure, because there are not three consecutive failures, whereas 200, 404, 404, 404 would. Likewise, a sequence of 404, 404, 404, 404 would not count as a link failing, because the baseline reading (404 in the first history entry) makes the status code unusable as a benchmark.

History parsing is only as good as the initial baseline. If the initial submission to Crossref fails, when we first sample the resource, it becomes very difficult to describe a degradation from a lower initial benchmark/baseline.

## Rebasing History
Shelob will rebase history when a page changes. This means that when a page changes, we will reset the history to the new baseline. This is to prevent a page from being flagged as changed when it has not been.

An example of this: if previously the baseline did _not_ contain the DOI or title, but one of these appears, this reading will be reset to be the new baseline.

## Prefix-wide enabling

The process for enabling a prefix is:

1. Write the prefix entry to S3
2. Iterate over the prefix and try each DOI
3. Enable Op Cit for each DOI in the prefix that passes initial checks

When re-appraising the prefix:

1. Iterate over the prefix and try each DOI
2. If Op Cit is not enabled, and there's a VOR, enable Op Cit

&copy; Crossref 2024
