import unittest
from unittest.mock import patch
import httpx
from src.shelob.shelob import Shelob


class TestFetch(unittest.TestCase):
    @patch("httpx.Client.get")
    def test_fetch(self, mock_get):
        """
        Test the fetch function
        """
        mock_get.return_value = httpx.Response(
            200,
            content=b"Test Content",
        )
        url = "https://example.com"
        timeout = 30
        response = Shelob.fetch(url, timeout)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.content, b"Test Content")

    @patch("src.shelob.shelob.Annotator.doi_to_md5")
    def test_build_s3_path(self, mock_doi_to_md5):
        mock_doi_to_md5.return_value = "md5_hash_of_locator"
        instance = Shelob()
        instance._bucket = "test-bucket"

        result = instance._build_s3_path(
            "https://example.com",
        )
        self.assertEqual("/works/md5_hash_of_locator.json", result)

    @patch("src.shelob.shelob.json.loads")
    @patch("src.shelob.shelob.Shelob._build_s3_path")
    @patch("src.shelob.shelob.Shelob._aws_connector")
    def test_get_record(
        self, mock_aws_connector, mock_build_s3_path, mock_json_loads
    ):
        instance = Shelob(aws_connector=mock_aws_connector)
        instance._bucket = "test-bucket"
        mock_build_s3_path.return_value = "s3/path/to/locator"
        mock_aws_connector.s3_obj_to_str.return_value = '[{"data": "value"}]'
        mock_json_loads.return_value = [{"data": "value"}]

        result = instance.get_history_record("https://example.com")
        self.assertEqual(result, [{"data": "value"}])

        mock_aws_connector.s3_obj_to_str.assert_called_with(
            bucket="shelob.research.crossref.org",
            s3_path="s3/path/to/locator",
            raise_on_fail=False,
        )

    @patch("src.shelob.shelob.Shelob._aws_connector")
    def test_write_record(self, mock_aws_connector):
        instance = Shelob(aws_connector=mock_aws_connector)
        instance._bucket = "test_bucket"
        record = '{"key": "value"}'
        endpoint = "s3/path/to/endpoint"

        instance.write_main_record(record=record, endpoint=endpoint)

        mock_aws_connector.push_json_to_s3.assert_called_once_with(
            json_obj=record, bucket="test_bucket", path=endpoint
        )
