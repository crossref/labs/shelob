import unittest
from unittest.mock import patch, MagicMock

from src.shelob.shelob import Shelob


class TestGetMime(unittest.TestCase):
    def test_return_pdf_mime_type(self):
        """
        Test that the mime checker function can recognise application/pdf
        """
        pdf_content = b"%PDF-1.5..."  # Truncated PDF content
        self.assertEqual(Shelob.get_mime(pdf_content), "application/pdf")

    @patch("html2text.html2text")
    def test_extract_text_from_html(self, mock_html2text):
        mock_html2text.return_value = "Extracted text"
        content = b"<html><body><p>Hello, world!</p></body></html>"
        result = Shelob._extract_text_from_html(content)
        self.assertEqual(result, "Extracted text")
        mock_html2text.assert_called_once_with(content.decode("utf-8"))

    @patch("src.shelob.shelob.PdfReader")
    def test_extract_text_from_pdf(self, mock_pdf_reader):
        # Set up mock PdfReader with pages
        mock_page = MagicMock()
        mock_page.extract_text.return_value = "Page text"
        mock_pdf_reader.return_value.pages = [mock_page, mock_page]

        content = b"%PDF-1.5..."
        result = Shelob._extract_text_from_pdf(content)
        self.assertEqual(result, "Page text\nPage text\n")


if __name__ == "__main__":
    unittest.main()
