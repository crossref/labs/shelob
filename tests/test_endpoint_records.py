import sys

sys.path.append("../src/shelob")

import unittest
import datetime
from unittest.mock import patch, MagicMock

from src.shelob.shelob import Shelob


class TestEndpointRecordMerger(unittest.TestCase):
    def setUp(self):
        self.merger = Shelob()

    def test_merge_with_empty_old_record(self):
        old_record = []
        new_record = {"datestamp": "2023-10-31", "tags": ["stable"]}
        result = self.merger._merge_endpoint_records(old_record, new_record)
        self.assertEqual(result, [new_record])

    def test_merge_with_stable_last_record(self):
        old_record = [
            {"datestamp": "2023-10-29", "tags": ["stable"]},
            {"datestamp": "2023-10-30", "tags": []},
        ]
        new_record = {"datestamp": "2023-10-31", "tags": ["stable"]}
        result = self.merger._merge_endpoint_records(old_record, new_record)
        self.assertNotIn("stable", result[0]["tags"])
        self.assertIn(new_record, result)

    def test_merge_without_stable_in_new_record(self):
        old_record = [
            {"datestamp": "2023-10-29", "tags": ["stable"]},
            {"datestamp": "2023-10-30", "tags": []},
        ]
        new_record = {"datestamp": "2023-10-31", "tags": []}
        result = self.merger._merge_endpoint_records(old_record, new_record)
        self.assertIn("stable", result[0]["tags"])
        self.assertIn(new_record, result)

    def test_merge_with_stable_far_behind(self):
        old_record = [
            {"datestamp": "2023-10-27", "tags": ["stable"]},
            {"datestamp": "2023-10-28", "tags": []},
            {"datestamp": "2023-10-29", "tags": []},
            {"datestamp": "2023-10-30", "tags": []},
        ]
        new_record = {"datestamp": "2023-10-31", "tags": ["stable"]}

        result = self.merger._merge_endpoint_records(old_record, new_record)

        self.assertNotIn("stable", result[0]["tags"])
        self.assertIn(new_record, result)

    def test_merge_with_stable_not_far_behind(self):
        old_record = [
            {"datestamp": "2023-10-28", "tags": ["stable"]},
            {"datestamp": "2023-10-29", "tags": []},
            {"datestamp": "2023-10-30", "tags": []},
        ]
        new_record = {"datestamp": "2023-10-31", "tags": ["stable"]}

        result = self.merger._merge_endpoint_records(old_record, new_record)

        self.assertNotIn("stable", result[0]["tags"])
        self.assertIn(new_record, result)

    def test_merge_with_stable_in_last_record(self):
        old_record = [
            {"datestamp": "2023-10-28", "tags": []},
            {"datestamp": "2023-10-29", "tags": []},
            {"datestamp": "2023-10-30", "tags": ["stable"]},
        ]
        new_record = {"datestamp": "2023-10-31", "tags": ["stable"]}

        result = self.merger._merge_endpoint_records(old_record, new_record)

        self.assertNotIn("stable", result[0]["tags"])
        self.assertIn(new_record, result)

    def test_merge_with_unsorted_old_record(self):
        old_record = [
            {"datestamp": "2023-10-29", "tags": []},
            {"datestamp": "2023-10-27", "tags": ["stable"]},
            {"datestamp": "2023-10-30", "tags": ["has_doi"]},
            {"datestamp": "2023-10-26", "tags": ["first"]},
        ]
        new_record = {"datestamp": "2023-10-31", "tags": ["stable"]}
        result = self.merger._merge_endpoint_records(old_record, new_record)

        self.assertNotIn("stable", result[1]["tags"])
        self.assertIn(new_record, result)

        self.assertEqual(
            [
                {"datestamp": "2023-10-26", "tags": ["first"]},
                {"datestamp": "2023-10-27", "tags": []},
                {"datestamp": "2023-10-29", "tags": []},
                {"datestamp": "2023-10-30", "tags": ["has_doi"]},
                {"datestamp": "2023-10-31", "tags": ["stable"]},
            ],
            result,
        )

    def test_set_tags(self):
        self.assertEqual(
            Shelob._set_tags(found_doi=True, found_title=True, record=[]),
            ["first", "stable", "has_doi", "has_title"],
        )
        self.assertEqual(
            Shelob._set_tags(found_doi=True, found_title=False, record=[]),
            ["first", "stable", "has_doi"],
        )
        self.assertEqual(
            Shelob._set_tags(found_doi=False, found_title=True, record=[]),
            ["first", "stable", "has_title"],
        )
        self.assertEqual(
            Shelob._set_tags(found_doi=False, found_title=False, record=[]),
            ["first", "stable"],
        )
        self.assertEqual(
            Shelob._set_tags(found_doi=True, found_title=True, record=["data"]),
            ["has_doi", "has_title"],
        )
        self.assertEqual(
            Shelob._set_tags(
                found_doi=True, found_title=False, record=["data"]
            ),
            ["has_doi"],
        )
        self.assertEqual(
            Shelob._set_tags(
                found_doi=False, found_title=True, record=["data"]
            ),
            ["has_title"],
        )
        self.assertEqual(
            Shelob._set_tags(
                found_doi=False, found_title=False, record=["data"]
            ),
            [],
        )

    @patch("datetime.datetime")
    def test_build_endpoint_snapshot(self, mock_datetime):
        mock_datetime.now.return_value = datetime.datetime(
            2023,
            1,
            1,
            0,
            0,
            0,
        )
        mock_datetime.now.return_value.timestamp.return_value = 1672531200.0

        result_success = Shelob._build_endpoint_snapshot(
            url="https://example.com",
            doi="10.1000/example",
            title="Example Title",
            datestamp=mock_datetime.now(),
            tags=["first", "stable"],
            checksum="abcdef123456",
            status_code=200,
            title_found=True,
            doi_found=True,
            page_type="Landing Page",
            pdf_url="https://example.com/pdf",
            pdf_strategy="strategy",
        )

        result_error = Shelob._build_endpoint_snapshot(
            url="https://example.com",
            doi="10.1000/example",
            title="Example Title",
            datestamp=mock_datetime.now(),
            tags=["first", "stable"],
            status_code=404,
        )

        self.assertEqual(
            result_success,
            {
                "datestamp": 1672531200.0,
                "url": "https://example.com",
                "doi": "10.1000/example",
                "title": "Example Title",
                "tags": ["first", "stable"],
                "checksum": "abcdef123456",
                "status_code": 200,
                "strategy": "httpx",
                "has_title": True,
                "has_doi": True,
                "page_type": "Landing Page",
                "pdf_url": "https://example.com/pdf",
                "pdf_strategy": "strategy",
            },
        )

        self.assertEqual(
            result_error,
            {
                "datestamp": 1672531200.0,
                "url": "https://example.com",
                "doi": "10.1000/example",
                "title": "Example Title",
                "tags": ["first", "stable"],
                "status_code": 404,
                "strategy": "httpx",
            },
        )

    @patch("src.shelob.shelob.datetime")
    @patch("src.shelob.shelob.hashlib.sha256")
    @patch("src.shelob.shelob.Shelob._build_endpoint_snapshot")
    @patch("src.shelob.shelob.Shelob._set_tags")
    @patch("src.shelob.shelob.Shelob._has_doi_and_title")
    @patch("src.shelob.shelob.Shelob.get_mime")
    @patch("src.shelob.shelob.Shelob.fetch")
    @patch("src.shelob.shelob.Shelob.get_metadata_record")
    @patch("src.shelob.shelob.Shelob._build_s3_path")
    @patch("src.shelob.shelob.Shelob._aws_connector")
    def test_snapshot(
        self,
        mock_aws_connector,
        mock_build_s3_path,
        mock_get_metadata_record,
        mock_fetch,
        mock_get_mime,
        mock_has_doi_and_title,
        mock_set_tags,
        mock_build_endpoint_snapshot,
        mock_sha256,
        mock_datetime,
    ):
        mock_datetime.now.return_value = datetime.datetime(2023, 1, 1)
        mock_build_s3_path.return_value = "s3/path/to/doi"
        mock_get_metadata_record.return_value = []
        response_mock = MagicMock()
        response_mock.status_code = 200
        response_mock.content = b"content"
        mock_fetch.return_value = response_mock
        mock_sha256().hexdigest.return_value = "sha256checksum"
        mock_get_mime.return_value = "application/pdf"
        mock_has_doi_and_title.return_value = (True, True)
        mock_set_tags.return_value = ["tag1", "tag2"]
        mock_build_endpoint_snapshot.return_value = {"endpoint": "data"}
        mock_aws_connector.s3_obj_to_str.return_value = "[]"

        instance = Shelob(aws_connector=mock_aws_connector)
        instance.snapshot(
            url="https://example.com", doi="doi123", title="Title"
        )

        mock_fetch.assert_called_once_with("https://example.com")
        mock_sha256.assert_called_with(response_mock.content)
        mock_get_mime.assert_called_once_with(response_mock.content)
        mock_has_doi_and_title.assert_called_once_with(
            content=response_mock.content,
            doi="doi123",
            title="Title",
            mime="application/pdf",
        )
        mock_set_tags.assert_called_once_with(True, True, [])
        mock_build_endpoint_snapshot.assert_called_once()

    @patch("src.shelob.shelob.datetime")
    @patch("src.shelob.shelob.hashlib.sha256")
    @patch("src.shelob.shelob.Shelob._build_endpoint_snapshot")
    @patch("src.shelob.shelob.Shelob._set_tags")
    @patch("src.shelob.shelob.Shelob._has_doi_and_title")
    @patch("src.shelob.shelob.Shelob.get_mime")
    @patch("src.shelob.shelob.Shelob.fetch")
    @patch("src.shelob.shelob.Shelob.get_metadata_record")
    @patch("src.shelob.shelob.Shelob._build_s3_path")
    @patch("src.shelob.shelob.Shelob._aws_connector")
    def test_snapshot_not_200(
        self,
        mock_aws_connector,
        mock_build_s3_path,
        mock_get_record,
        mock_fetch,
        mock_get_mime,
        mock_has_doi_and_title,
        mock_set_tags,
        mock_build_endpoint_snapshot,
        mock_sha256,
        mock_datetime,
    ):
        mock_datetime.now.return_value = datetime.datetime(2023, 1, 1)
        mock_build_s3_path.return_value = "s3/path/to/doi"
        mock_get_record.return_value = []
        response_mock = MagicMock()
        response_mock.status_code = 404
        response_mock.content = b"content"
        mock_fetch.return_value = response_mock
        mock_sha256().hexdigest.return_value = "sha256checksum"
        mock_get_mime.return_value = "application/pdf"
        mock_has_doi_and_title.return_value = (True, True)
        mock_set_tags.return_value = ["tag1", "tag2"]
        mock_build_endpoint_snapshot.return_value = {"endpoint": "data"}

        instance = Shelob(aws_connector=mock_aws_connector)
        instance.snapshot(
            url="https://example.com", doi="doi123", title="Title"
        )

        mock_fetch.assert_called_once_with("https://example.com")

        mock_build_endpoint_snapshot.assert_called_once()
