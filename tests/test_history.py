import unittest
from unittest.mock import MagicMock

from src.shelob.shelob import Shelob


class TestResourceChecker(unittest.TestCase):
    def setUp(self):
        self.checker = Shelob()
        self.checker.get_metadata_record = MagicMock()
        self.checker._get_history = MagicMock()
        self.checker.write_shelob_record = MagicMock()
        self.checker.snapshot = MagicMock()
        self.checker.get_current_status = MagicMock()

    def test_sort_history(self):
        history = [
            {"datestamp": "2021-01-01"},
            {"datestamp": "2020-01-01"},
            {"datestamp": "2022-01-01"},
        ]
        sorted_history = self.checker.sort_history(history)
        self.assertEqual(sorted_history[0]["datestamp"], "2020-01-01")
        self.assertEqual(sorted_history[1]["datestamp"], "2021-01-01")
        self.assertEqual(sorted_history[2]["datestamp"], "2022-01-01")

    def test_filter_history(self):
        history = [{"url": "url1"}, {"url": "url2"}, {"url": "url1"}]
        filtered_history = self.checker.filter_history(history, "url1")
        self.assertEqual(len(filtered_history), 2)
        self.assertEqual(filtered_history[0]["url"], "url1")
        self.assertEqual(filtered_history[1]["url"], "url1")

    def test_has_died_insufficient_data(self):
        history = [{"url": "url1", "status_code": 200}]
        result = self.checker.has_died(history, "url1")
        self.assertFalse(result)

    def test_has_died_resource_alive(self):
        history = [
            {
                "url": "url1",
                "status_code": 200,
                "has_title": True,
                "has_doi": True,
                "datestamp": "2021-01-01",
            },
            {
                "url": "url1",
                "status_code": 404,
                "has_title": False,
                "has_doi": False,
                "datestamp": "2021-01-02",
            },
            {
                "url": "url1",
                "status_code": 200,
                "has_title": True,
                "has_doi": True,
                "datestamp": "2021-01-03",
            },
            {
                "url": "url1",
                "status_code": 404,
                "has_title": False,
                "has_doi": False,
                "datestamp": "2021-01-04",
            },
            {
                "url": "url1",
                "status_code": 404,
                "has_title": False,
                "has_doi": False,
                "datestamp": "2021-01-05",
            },
        ]
        result = self.checker.has_died(history, "url1")
        self.assertFalse(result)

    def test_rebase_status_alive(self):
        history = [
            {
                "url": "url1",
                "status_code": 404,
                "has_title": False,
                "has_doi": False,
                "datestamp": "2021-01-01",
            },
            {
                "url": "url1",
                "status_code": 404,
                "has_title": False,
                "has_doi": False,
                "datestamp": "2021-01-02",
            },
            {
                "url": "url1",  # here comes the rebase
                "status_code": 200,
                "has_title": True,
                "has_doi": False,
                "datestamp": "2021-01-03",
            },
            {
                "url": "url1",
                "status_code": 404,
                "has_title": False,
                "has_doi": False,
                "datestamp": "2021-01-04",
            },
            {
                "url": "url1",
                "status_code": 404,
                "has_title": False,
                "has_doi": False,
                "datestamp": "2021-01-05",
            },
        ]
        result = self.checker.has_died(history, "url1")
        self.assertFalse(result)

    def test_rebase_title_alive(self):
        history = [
            {
                "url": "url1",
                "status_code": 200,
                "has_title": False,
                "has_doi": True,
                "datestamp": "2021-01-01",
            },
            {
                "url": "url1",
                "status_code": 404,
                "has_title": False,
                "has_doi": False,
                "datestamp": "2021-01-02",
            },
            {
                "url": "url1",  # here comes the rebase
                "status_code": 200,
                "has_title": True,
                "has_doi": False,
                "datestamp": "2021-01-03",
            },
            {
                "url": "url1",
                "status_code": 404,
                "has_title": False,
                "has_doi": False,
                "datestamp": "2021-01-04",
            },
            {
                "url": "url1",
                "status_code": 404,
                "has_title": False,
                "has_doi": False,
                "datestamp": "2021-01-05",
            },
        ]
        result = self.checker.has_died(history, "url1")
        self.assertFalse(result)

    def test_rebase_doi_alive(self):
        history = [
            {
                "url": "url1",
                "status_code": 200,
                "has_title": True,
                "has_doi": False,
                "datestamp": "2021-01-01",
            },
            {
                "url": "url1",
                "status_code": 404,
                "has_title": False,
                "has_doi": False,
                "datestamp": "2021-01-02",
            },
            {
                "url": "url1",  # here comes the rebase
                "status_code": 200,
                "has_title": False,
                "has_doi": True,
                "datestamp": "2021-01-03",
            },
            {
                "url": "url1",
                "status_code": 404,
                "has_title": False,
                "has_doi": False,
                "datestamp": "2021-01-04",
            },
            {
                "url": "url1",
                "status_code": 404,
                "has_title": False,
                "has_doi": False,
                "datestamp": "2021-01-05",
            },
        ]
        result = self.checker.has_died(history, "url1")
        self.assertFalse(result)

    def test_short_rebase_doi_alive(self):
        history = [
            {
                "url": "url1",
                "status_code": 200,
                "has_title": True,
                "has_doi": False,
                "datestamp": "2021-01-01",
            },
            {
                "url": "url1",  # here comes the rebase
                "status_code": 200,
                "has_title": False,
                "has_doi": True,
                "datestamp": "2021-01-03",
            },
        ]
        result = self.checker.has_died(history, "url1")
        self.assertFalse(result)

    def test_has_died_resource_dead(self):
        history = [
            {
                "url": "url1",
                "status_code": 200,
                "has_title": False,
                "has_doi": False,
                "datestamp": "2020-01-01",
            },
            {
                "url": "url1",
                "status_code": 404,
                "has_title": False,
                "has_doi": False,
                "datestamp": "2021-01-01",
            },
            {
                "url": "url1",
                "status_code": 404,
                "has_title": False,
                "has_doi": False,
                "datestamp": "2021-01-02",
            },
            {
                "url": "url1",
                "status_code": 404,
                "has_title": False,
                "has_doi": False,
                "datestamp": "2021-01-03",
            },
        ]
        result = self.checker.has_died(history, "url1")
        self.assertTrue(result)

    def test_multiple_resolution_enabled(self):
        # Scenario where get_current_status returns a non-empty record
        self.checker.get_current_status.return_value = {"key": "value"}
        self.assertTrue(
            self.checker.is_multiple_resolution_enabled("sample_doi")
        )

    def test_no_record_found(self):
        # Scenario where get_current_status returns None
        self.checker.get_current_status.return_value = None
        self.assertFalse(
            self.checker.is_multiple_resolution_enabled("sample_doi")
        )

        # Scenario where get_current_status returns an empty record
        self.checker.get_current_status.return_value = {}
        self.assertFalse(
            self.checker.is_multiple_resolution_enabled("sample_doi")
        )


if __name__ == "__main__":
    unittest.main()
