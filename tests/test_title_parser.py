import unittest
from unittest.mock import patch

from src.shelob.shelob import Shelob


class TestTitleParser(unittest.TestCase):
    def setUp(self):
        self.shelob = Shelob()

    def test_basic_title(self):
        title = "The Title of the Page"
        content = "This is \n The Title of the Page \n by Martin Paul Eve"

        self.assertTrue(self.shelob._test_for_title(content, title))

    def test_multi_space_title(self):
        title = "The Title of the Page"
        content = (
            "This is \n The     Title of the       Page \n    "
            "by Martin Paul Eve"
        )

        self.assertTrue(self.shelob._test_for_title(content, title))

    def test_line_break_title(self):
        title = "The Title of the Page"
        content = (
            "This is \n The   \n    Title of the   \n    Page \n    "
            "by Martin Paul Eve"
        )

        self.assertTrue(self.shelob._test_for_title(content, title))

    def test_failure_break_title(self):
        title = "The Title of the Page"
        content = (
            "This is \n The   \n    of the   \n    Page \n    "
            "by Martin Paul Eve"
        )

        self.assertFalse(self.shelob._test_for_title(content, title))

    def test_sanitize(self):
        """
        Test that the sanitize function removes multiple spaces and newlines
        """
        text = "Example\nText With   Multiple\n\nSpaces"
        expected_result = "exampletext with multiplespaces"
        self.assertEqual(self.shelob._sanitize(text), expected_result)

    @patch.object(Shelob, "_extract_text_from_pdf")
    @patch.object(Shelob, "_test_for_title")
    def test_has_doi_and_title_pdf(
        self, mock_test_for_title, mock_extract_text_from_pdf
    ):
        mock_extract_text_from_pdf.return_value = (
            "Some content with DOI 10.1000/example and title Example Title"
        )
        mock_test_for_title.return_value = True
        found_doi, found_title = Shelob._has_doi_and_title(
            b"PDF content",
            "10.1000/example",
            "Example Title",
            mime="application/pdf",
        )
        self.assertTrue(found_doi)
        self.assertTrue(found_title)

    @patch.object(Shelob, "_extract_text_from_html")
    @patch.object(Shelob, "_test_for_title")
    def test_has_doi_and_title_html(
        self, mock_test_for_title, mock_extract_text_from_html
    ):
        mock_extract_text_from_html.return_value = (
            "Some content with DOI 10.1000/example and title Example Title"
        )
        mock_test_for_title.return_value = False
        found_doi, found_title = Shelob._has_doi_and_title(
            b"HTML content",
            "10.1000/example",
            "Example Title",
            mime="text/html",
        )
        self.assertTrue(found_doi)
        self.assertFalse(found_title)

    @patch.object(Shelob, "_test_for_title")
    def test_has_doi_and_title_other_mime(self, mock_test_for_title):
        mock_test_for_title.return_value = False
        found_doi, found_title = Shelob._has_doi_and_title(
            b"Some other content",
            "10.1000/example",
            "Example Title",
            mime="application/other",
        )
        self.assertFalse(found_doi)
        self.assertFalse(found_title)


if __name__ == "__main__":
    unittest.main()
